from random import choice, randint

TYPES = ["periph", "forloop", "ifelsemerge", "storeimm", "loadstore"]
MAX_LEVEL = 3

def indent(s, level):
    lines = s.split("\n")
    return "\n".join(("\t" * level) + line for line in lines)

class CFGNode:
    def __init__(self, typ):
        self.typ = typ

class PeriphNode(CFGNode):
    def __init__(self, peripheral_name, target_state):
        CFGNode.__init__(self, TYPES[0])
        self.peripheral_name = peripheral_name
        self.target_state = target_state

    def to_c(self, platform):
        new_platform = platform.copy()
        sequence, new_platform = new_platform.peripheral(self.peripheral_name).force_state(self.target_state, new_platform)
        code = ("/* Periph node: %s.force_state(%s) */\n" % (self.peripheral_name, self.target_state))
        code += "\n".join(i + ';' for i in sequence)
        return code, new_platform

class ForLoopNode(CFGNode):
    def __init__(self, iterations, variable_name, nodes):
        CFGNode.__init__(self, TYPES[1])
        self.iterations = iterations
        self.nodes = nodes
        self.variable_name = variable_name

    def to_c(self, platform):
        # 1. Run a loop iteration on a clone platform
        clone_platform = platform.copy()
        _, new_clone_platform = format_c(self.nodes, clone_platform)

        # 2. Before the "for" statement, prepare the platform so that its state is the same as after running the for-loop
        prefix_code = platform.copy().force(new_clone_platform)

        # 3. Build the loop based on the new platform
        inner_code, new_platform = format_c(self.nodes, new_clone_platform)

        #code = "/* For loop node */\n"

        code  = prefix_code + "\n"
        code += ("\nfor(size_t %s = 0; %s < 0x%x; ++%s)\n{\n" % (self.variable_name, self.variable_name, self.iterations, self.variable_name))
        code += indent(inner_code, 1) + "\n}\n"

        return code, new_platform

class IfElseMergeNode(CFGNode):
    def __init__(self, condition, nodes_if, nodes_else):
        CFGNode.__init__(self, TYPES[2])
        self.condition = condition
        self.nodes_if = nodes_if
        self.nodes_else = nodes_else

    def to_c(self, platform):
        condition_code, precondition = self.condition
        seq_pre, new_platform = precondition(platform.copy())
        code_pre = "\n".join(i + ';' for i in seq_pre)

        code_if, new_platform_if     = format_c(self.nodes_if, new_platform)
        code_else, new_platform_else = format_c(self.nodes_else, new_platform)

        for pname in platform.get_peripheral_names():
            # 1. Try to set the else-platform like the if-platform
            packed = new_platform_else.peripheral(pname).force_state(new_platform_if.peripheral(pname).state, new_platform_else)
            if packed != None:
                seq, new_platform_else = packed
                code_else += "\n" + "\n".join(i + ';' for i in seq)
            else: # Cannot set the peripheral of the else-platform
                # 2. Try to set the if-platform like the else-platform
                packed = new_platform_if.peripheral(pname).force_state(new_platform_else.peripheral(pname).state, new_platform_else)
                seq, new_platform_if = packed
                code_if += "\n" + "\n".join(i + ';' for i in seq)


        #code = "/* If/else/merge node */\n"
        code  = code_pre + "\n"

        code += ("if (%s)\n{\n" % condition_code)
        code += indent(code_if, 1) + "\n"


        code += "\n}\nelse\n{\n"
        code += indent(code_else, 1) + "\n"

        code += "\n}\n"
        return code, new_platform_if

class StoreImmNode(CFGNode):
    def __init__(self, variable_name, value):
        CFGNode.__init__(self, TYPES[3])
        self.variable_name = variable_name
        self.value = value

    def to_c(self, platform):
        code = ("%s = 0x%x;" % (self.variable_name, self.value))
        return code, platform

class LoadStoreNode(CFGNode):
    def __init__(self, dst_variable, src_variable):
        CFGNode.__init__(self, TYPES[4])
        self.dst_variable = dst_variable
        self.src_variable = src_variable

    def to_c(self, platform):
        code = ("%s = %s;" % (self.dst_variable, self.src_variable))
        return code, platform

def gen_for_variable_name(level):
    return chr(ord('a') + level - 1)

def get_interesting_peripherals(platform):
    return list(p for p in platform.get_peripheral_names() if len(platform.peripheral(p).get_transition_states()) > 0)

def peripheral_transition_factory(platform):
    interesting_peripherals = get_interesting_peripherals(platform)
    if len(interesting_peripherals) == 0:
        raise Exception("No more interesting peripheral!")
    peripheral_name = choice(interesting_peripherals)

    target_state = choice(list(platform.peripheral(peripheral_name).get_transition_states()))
    return PeriphNode(peripheral_name, target_state)

def for_loop_factory(platform, allocator, live_local_variables, condition_factory, level):
    N_ITERATIONS_MAX = 1024

    iterations = randint(1, N_ITERATIONS_MAX)
    variable_name = gen_for_variable_name(level)

    #tmp = peripheral_transition_factory(platform)
    #invariant = (lambda p: p.peripheral(tmp.peripheral_name).force_state(tmp.target_state, p))

    subcfg = cfg_factory(platform, allocator, condition_factory, set(live_local_variables), level)

    return ForLoopNode(iterations, variable_name, subcfg)

def if_else_merge_factory(platform, allocator, live_local_variables, condition_factory, level):
    tmp = peripheral_transition_factory(platform)
    all_variables = (live_local_variables | allocator.all_variables())
    return IfElseMergeNode(condition_factory(all_variables), cfg_factory(platform, allocator, condition_factory, live_local_variables, level), cfg_factory(platform, allocator, condition_factory, live_local_variables, level))

def store_imm_factory(platform, allocator):
    variable_name = allocator.pick_or_allocate()
    value = randint(0, 0xffff)
    return StoreImmNode(variable_name, value)

def load_store_factory(platform, allocator, live_local_variables):
    dst_variable = allocator.pick_or_allocate()
    src_variable = allocator.pick_or_allocate(live_local_variables)
    return LoadStoreNode(dst_variable, src_variable)

def possible_node_types(level):
    ret = set(range(5))
    if level+1 >= MAX_LEVEL:
        ret -= {1, 2}
    return sorted(ret)

def cfg_factory(platform, allocator, condition_factory, live_local_variables, level=0):
    N_ITEMS = randint(4, 16)

    if level >= MAX_LEVEL:
        return list()
    types = possible_node_types(level)

    nodes = list()

    for i in range(N_ITEMS):
        typ = choice(types)
        if typ == 0: # periph
            node = peripheral_transition_factory(platform)
        elif typ == 1: # for loop
            node = for_loop_factory(platform, allocator, live_local_variables, condition_factory, level+1)
        elif typ == 2: # if/else/merge
            node = if_else_merge_factory(platform, allocator, live_local_variables, condition_factory, level+1)
        elif typ == 3: # Store immediate value
            node = store_imm_factory(platform, allocator)
        elif typ == 4: # Load and store
            node = load_store_factory(platform, allocator, live_local_variables)
        else:
            raise Exception("CFG factory: unknown node type")
        nodes.append(node)

    return nodes

def format_c(nodelist, platform):
    code = ""
    for node in nodelist:
        #code += ("/*\n%s\n*/\n" % platform.dump_state())
        packed = node.to_c(platform)
        if packed is not None:
            tmp_code, new_platform = packed
            code += tmp_code + "\n"
            platform = new_platform
    return code, platform

