import random
import time
import sys

import cfg
import msp430_expfr5739 as msp

if len(sys.argv) > 1:
    seed = int(sys.argv[1])
else:
    seed = int(time.time())
random.seed(seed)

print("// Seed: %d\n" % seed)

def init_section(required_peripherals):
    mock_platform = msp.platform_factory()

    code = ""
    for peripheral in required_peripherals:
        p = mock_platform.peripheral(peripheral)
        init_transition = next(t for t in p.transitions if t[0] == "uninit")
        tmp = p.force_state(init_transition[2], mock_platform, False)
        if tmp is not None:
            seq, mock_platform = tmp
            code += "\n".join(i + ';' for i in seq) + "\n"
    return code

platform = msp.platform_factory()

# 0: SRAM
# 1: NVRAM
allocator = msp.StaticAllocator([4, 32])

cf = cfg.cfg_factory(platform, allocator, msp.condition_factory, set())
code_body, platform = cfg.format_c(cf, platform)
#code_init = init_section(platform.get_required_peripherals())

headers = ["msp430", "stdint", "kernel","drivers/clock", "drivers/spi", "drivers/cc2500", "drivers/accelerometer", "drivers/port", "drivers/temperature", "drivers/utils"]

print("\n".join("#include <%s.h>" % i for i in headers))

print(allocator.format_c())
print()

print("int main(void)\n{\n")
#print(cfg.indent(code_init, 1))
print(cfg.indent(code_body, 1))
print(cfg.indent("for(;;);\nreturn 0;", 1))
print("}\n")
