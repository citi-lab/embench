def find_path(noinit, transitions, used_transitions, start, goal, sequence, platform):
    if start == goal:
        return sequence, platform

    t = sorted(i for i in transitions - used_transitions if i[0] == start)
    possibilities = list()
    for transition in t:
        tmpseq = list(sequence)
        new_platform = platform

        dependency = transition[3]
        if dependency is not None:
            new_platform = platform.copy()
            subseq, new_platform = dependency(new_platform)
            tmpseq.extend(subseq)
        tmpseq.append(transition[1])
        tmp = find_path(noinit, transitions, used_transitions | {transition}, transition[2], goal, tmpseq, new_platform)
        if tmp is not None:
            possibilities.append(tmp)
    if len(possibilities) > 0:
        possibilities.sort(key=(lambda x: len(x[0])))
        return possibilities[0]
    return None

class Peripheral:
    def __init__(self, name, transitions):
        self.name = name
        self.state = "uninit"
        self.transitions = transitions
        self.states = self.get_states()

    def copy(self):
        p = Peripheral(self.name, self.transitions)
        p.state = self.state
        return p

    def get_states(self):
        return sorted((set(i[0] for i in self.transitions) | set(i[2] for i in self.transitions)) - {"init", "uninit"})

    def get_transition_states(self):
        return sorted((set(i[2] for i in self.transitions) - {"init", "uninit", self.state}))

    def force_state(self, goal, platform, noinit=True):
        packed = find_path(noinit, set(self.transitions), set(), self.state, goal, list(), platform)
        if packed is not None:
            _, platform = packed
            platform.peripheral(self.name).state = goal
        return packed

class Platform:
    def __init__(self):
        self.peripherals = dict()

    def copy(self):
        p = Platform()
        p.peripherals = dict((name, per.copy()) for name, per in self.peripherals.items())
        return p

    def add_peripheral(self, peripheral):
        self.peripherals[peripheral.name] = peripheral

    def peripheral(self, name):
        return self.peripherals[name]

    def dump_state(self):
        return "\n".join("Peripheral '%s': state = %s" % (p.name, p.state) for p in self.peripherals.values())

    def get_peripheral_names(self):
        return sorted(p for p in self.peripherals.keys() if len(self.peripherals[p].get_states()) > 0)

    def force(self, other):
        code = ""
        platform = self
        for peripheral_name in self.peripherals.keys():
            seq, platform = self.peripherals[peripheral_name].force_state(other.peripheral(peripheral_name).state, platform)
            c = "\n".join(i + ';' for i in seq)
            code += c + "\n"
        return code

    def get_required_peripherals(self):
        return sorted(p for p in self.peripherals.keys() if self.peripherals[p].state != "uninit")

