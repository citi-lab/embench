import model
import random

class StaticAllocator:
    def __init__(self, maxima):
        self.pools = list([] for i in range(2))
        self.maxima = maxima
        self.n = 0

    def __not_full(self):
        return list(pool for i, pool in enumerate(self.pools) if len(pool) < self.maxima[i])

    def __not_empty(self):
        return list(pool for i, pool in enumerate(self.pools) if len(pool) > 0)

    def __force_pick(self, collections):
        return random.choice(list(random.choice(collections)))

    def allocate(self):
        not_full = self.__not_full()
        if len(not_full) == 0:
            raise Exception("Allocator is full %s" % str(self.pools))
        pool = random.choice(not_full)
        variable_name = ("v_%x" % self.n)
        pool.append(variable_name)
        self.n += 1
        return variable_name

    def pick_variable(self):
        not_empty = self.__not_empty()
        if len(not_empty) == 0:
            raise Exception("Allocator is empty")
        return self.__force_pick(not_empty)

    def pick_or_allocate(self, complementary = list()):
        not_full = self.__not_full()
        not_empty = self.__not_empty()
        if len(complementary) > 0:
            not_empty.append(complementary)

        if len(not_empty) == 0 or (random.randint(0, 1) == 0 and len(not_full) > 0):
            return self.allocate()
        return self.__force_pick(not_empty)

    def all_variables(self):
        ret = set()
        for pool in self.pools:
            ret |= set(pool)
        return ret

    def format_c(self):
        sram = "static volatile unsigned int " + ", ".join(self.pools[0]) + ";"
        nvram = "static volatile unsigned int __attribute__((persistent)) " + ", ".join(self.pools[1]) + ";"

        ret = ""
        if len(self.pools[0]) > 0:
            ret += sram + "\n"
        if len(self.pools[1]) > 0:
            ret += nvram + "\n"
        return ret

def clock_factory(name = "clock"):
    transitions = [
        ("uninit", "syt_clk_drv_init()", "init", None)
    ]
    return model.Peripheral(name, transitions)

def spi_factory(name = "spi"):
    clock_force_config = (lambda p: p.peripheral("clock").force_state("init", p))
    transitions = [
        ("uninit", "syt_spi_init()", "init", clock_force_config),
        ("init", "syt_spi_configure(&spiSettings_default_config)", "config_default", clock_force_config)
    ]

    return model.Peripheral(name, transitions)

def cc2500_factory(name = "cc2500"):
    spi_force_config = (lambda p: p.peripheral("spi").force_state("config_default", p))
    transitions = [
        ("uninit", "syt_cc2500_init()", "init", spi_force_config),
        ("init", "syt_cc2500_configure(&rfSettings_default_config, 1)", "idle", spi_force_config),
        ("idle", "syt_cc2500_idle()", "idle", spi_force_config),
        ("rx", "syt_cc2500_idle()", "idle", spi_force_config),
        ("idle", "syt_cc2500_sleep()", "sleep", spi_force_config),
        ("rx", "syt_cc2500_sleep()", "sleep", spi_force_config),
        ("sleep", "syt_cc2500_wakeup()", "idle", spi_force_config),
        ("idle", "syt_cc2500_rx_enter()", "rx", spi_force_config)
    ]

    return model.Peripheral(name, transitions)

def accelerometer_factory(name = "accel"):
    transitions = [
        ("uninit", "accelerometer_init()", "off", None),
        ("off", "accelerometer_on()", "on", None),
        ("on", "accelerometer_off()", "off", None)
    ]

    return model.Peripheral(name, transitions)

def port_factory(name = "port"):
    transitions = [
        ("uninit", "syt_prt_drv_init()", "init", None)
    ]

    return model.Peripheral(name, transitions)

def temperature_factory(name = "temperature"):
    transitions = [
        ("uninit", "syt_tmp_drv_init()", "init", None)
    ]

    return model.Peripheral(name, transitions)

def timer_factory(name = "timer"):
    clock_force_config = (lambda p: p.peripheral("clock").force_state("init", p))
    transitions = [
        ("uninit", "syt_timer_drv_init()", "init", clock_force_config),
        ("init", "syt_timer_set_source(TIMERA, 0, SMCLK, DIV2); syt_timer_set_mode(TIMERA, 0, CONTINUOUS)", "config", clock_force_config)
    ]

    return model.Peripheral(name, transitions)

def condition_factory(all_variables):
    # Condition: A operator B
    # Todo: other peripherals (temperature, radio?)

    to_test = sorted(all_variables | {"syt_timer_get_counter(TIMERA, 0)"})

    operator = random.choice(["==", "<", ">"])
    operand_A = random.choice(list(to_test))

    possible_B = all_variables - {operand_A}
    if len(possible_B) == 0 or random.randint(0, 1) == 0:
        operand_B = ("0x%x" % random.randint(0, 0xffff))
    else:
        operand_B = random.choice(sorted(possible_B))
    code = ("%s %s %s" % (operand_A, operator, operand_B))

    # TODO: change precondition!
    precondition = (lambda p: p.peripheral("timer").force_state("config", p))
    return code, precondition

def platform_factory():
    platform = model.Platform()

    #cpu = model.Peripheral("cpu", list())
    clock = clock_factory()
    spi = spi_factory()
    cc2500 = cc2500_factory()
    accel = accelerometer_factory()
    port = port_factory()
    temperature = temperature_factory()
    timer = timer_factory()

    #platform.add_peripheral(cpu)
    platform.add_peripheral(clock)
    platform.add_peripheral(spi)
    platform.add_peripheral(cc2500)
    platform.add_peripheral(accel)
    platform.add_peripheral(port)
    platform.add_peripheral(temperature)
    platform.add_peripheral(timer)
    
    return platform
